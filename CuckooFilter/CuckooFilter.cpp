﻿#include <iostream>
#include <string>
#include <fstream>
#include <unordered_map>
#include <memory>


class CuckooFilter {
public:
    CuckooFilter(int m, int b, size_t max_num_kicks) : b_(b),
        max_num_kicks_(max_num_kicks) {

        m_ = 1;
        while (m_ < m) {
            m_ = m_ << 1;
        }

        buckets = new uint32_t[m_];

        for (size_t i = 0; i < m_; i++) {
            buckets[i] = 0;
        }
    }

    ~CuckooFilter() {
        delete[] buckets;
    }

    /// <summary>
    /// Вносит элемент в фильтр.
    /// </summary>
    /// <param name="video"> Элемент. </param>
    /// <returns> Истина, если элемент успешно вставлен, ложь - иначе. </returns>
    bool Insert(std::string video) {
        unsigned char f = Fingerprint(video);

        // Вычисляем номера потенциальных бакетов.
        uint64_t i1 = HashItem(video);
        uint64_t i2 = i1 ^ HashFingerprint(f);

        // Если не удалось вставить в один из тех двух бакетов.
        if (!TryInsertToBucket(i1, f) && !TryInsertToBucket(i2, f)) {

            // Запускаем алгоритм кукушки.
            if (!Coocking(i1, i2, f)) {
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Проверяет, есть ли элемент в фильтре.
    /// </summary>
    /// <param name="video"> Элемент. </param>
    /// <returns> Истина, если элемент есть в фильтре, ложь - иначе. </returns>
    bool Lookup(std::string video) {
        unsigned char f = Fingerprint(video);

        // Вычисляем номера потенциальных бакетов.
        uint64_t i1 = HashItem(video);
        uint64_t i2 = i1 ^ HashFingerprint(f);

        // Если нет ни в одном из бакетов, о его нет в фильтре.
        if (!FindEntryInBucket(buckets[i1], f + (1 << 7)) &&
            !FindEntryInBucket(buckets[i2], f + (1 << 7))) {
            return false;
        }

        return true;
    }

private:
    int b_; // Количество ячеек в одном бакете.
    uint64_t m_; // Количество бакетов в фильтре.
    uint64_t max_num_kicks_; // Количество попыток вставки в алгоритме кукушки.

    uint32_t* buckets; // Бакеты.

    /// <summary>
    /// Хэширует строку (сопоставляет ей её числовой эквивалент).
    /// </summary>
    /// <param name="video"> Строка для хэширования. </param>
    /// <returns> Хэш. </returns>
    uint64_t HashString(std::string video) {
        // Простое число, примерно равное количеству элементов в алфавите - 257.
        // (хоть у нас и 62 элемента в алфавите, тут нужно учитывать всю ASCII).
        int hash_constant = 257;
        uint64_t hash = 0, hash_const_pow = 1;

        for (size_t i = 0; i < video.length(); ++i) {
            hash += video[i] * hash_const_pow;
            hash_const_pow *= hash_constant;
        }

        return hash;
    }

    /// <summary>
    /// Хэширует элемент в 7-битный fingerprint.
    /// </summary>
    /// <param name="video"> Элемент. </param>
    /// <returns> Хэш. </returns>
    unsigned char Fingerprint(std::string video) {
        uint64_t string_hash = HashString(video);
        string_hash &= ((1u << 7u) - 1u);

        return string_hash;
    }

    /// <summary>
    /// Хэширует элемент уже непосредственно необходимой функцией хэширования.
    /// </summary>
    /// <param name="video"> Элемент для хэширования. </param>
    /// <returns> Хэш. </returns>
    uint64_t HashItem(std::string video) {
        std::hash<std::string> video_hash;

        return video_hash(video) % m_;
    }

    /// <summary>
    /// Хэширует число уже непосредственно необходимой функцией хэширования.
    /// </summary>
    /// <param name="video"> Строка для хэширования. </param>
    /// <returns> Хэш. </returns>
    uint64_t HashFingerprint(unsigned char fingerprint) {
        std::hash<unsigned char> fingerprint_hash;
        return fingerprint_hash(fingerprint) % m_;
    }

    /// <summary>
    /// Получает значение ячейки бакета по её индексу.
    /// </summary>
    /// <param name="bucket"> Бакет. </param>
    /// <param name="entry_num"> Индекс ячейки. </param>
    /// <returns> Значение ячейки. </returns>
    unsigned char GetEntry(uint32_t bucket, uint32_t entry_num) {
        bucket = bucket << (32u - 8u * entry_num);
        bucket = bucket >> 24u;
        return (unsigned char)bucket;
    }

    /// <summary>
    /// Ищет индекс первой свободной ячейки в бакете.
    /// </summary>
    /// <param name="bucket"> Бакет. </param>
    /// <returns> Индекс первой пустой ячейки. </returns>
    int GetNumOfEmptyEntry(uint32_t bucket) {
        int entry_num = 1;

        while (GetEntry(bucket, entry_num) != 0 && entry_num <= b_) {
            entry_num++;
        }

        return entry_num;
    }

    /// <summary>
    /// Вносит значение fingerprint'a в бакет по индексу.
    /// </summary>
    /// <param name="bucket"> Бакет. </param>
    /// <param name="entry_num"> Индекс ячейки. </param>
    /// <param name="fingerprint"> Фингерпринт. </param>
    /// <returns> Преобразованный бакет. </returns>
    uint32_t InsertFingerprint(uint32_t bucket, uint32_t entry_num, unsigned char fingerprint) {
        uint32_t mask = ((1u << 8u) - 1u) << (8u * (entry_num - 1u));
        mask = UINT32_MAX - mask;

        bucket &= mask;
        bucket ^= ((uint32_t)fingerprint + (1u << 7u)) << (8u * (entry_num - 1u));
        return bucket;
    }

    /// <summary>
    /// Ищет ячейку в бакете.
    /// </summary>
    /// <param name="bucket"> Бакет. </param>
    /// <param name="entry"> Ячейка. </param>
    /// <returns> Истина, если такая ячейка есть в бакете, ложь - иначе. </returns>
    bool FindEntryInBucket(uint32_t bucket, unsigned char entry) {
        int entry_num = 1;
        unsigned char curr_entry = GetEntry(bucket, entry_num);

        while (curr_entry != 0 && (curr_entry != entry) && entry_num < b_ + 1) {
            entry_num++;
            curr_entry = GetEntry(bucket, entry_num);
        }

        if (entry_num < b_ + 1 && curr_entry != 0) {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Пробует вставить fingerprint в бакет без необходимости выталкивать другой элемент.
    /// </summary>
    /// <param name="bucket_num"> Номер бакета. </param>
    /// <param name="fingerprint"> Фингерпринт. </param>
    /// <returns> Истина, если получилось вставить, без необходимости выталкивать
    /// другой элемент, ложь - иначе. </returns>
    bool TryInsertToBucket(uint64_t bucket_num, unsigned char fingerprint) {
        // Если такая ячейка уже есть в бакете, то не вставляем.
        if (FindEntryInBucket(buckets[bucket_num], fingerprint)) {
            return true;
        }

        int entry_num = GetNumOfEmptyEntry(buckets[bucket_num]);

        if (entry_num < b_ + 1) {
            buckets[bucket_num] = InsertFingerprint(buckets[bucket_num], entry_num, fingerprint);
            return true;
        }

        // Придется выталкивать элемент...
        return false;
    }

    /// <summary>
    /// Реализует алгоритм кукушки.
    /// </summary>
    /// <param name="i1"> Первый бакет, в котором может располагаться данный fingerprint. </param>
    /// <param name="i2"> Второй бакет, в котором может располагаться данный fingerprint. </param>
    /// <param name="fingerprint"> Фингерпринт. </param>
    /// <returns> Истина, если в итоге получилось вставить элемент, ложь - иначе. </returns>
    bool Coocking(uint64_t i1, uint64_t i2, unsigned char fingerprint) {
        // Выбираем рандомный bucket.
        uint64_t i3 = (rand() % 2 == 1) ? i1 : i2;

        for (size_t i = 0; i < max_num_kicks_; i++) {
            // Рандомно выбираем entry.
            int entry_num = rand() % b_ + 1;

            int entry = GetEntry(buckets[i3], entry_num);

            // Меняем её содержимое с fingerprint.
            buckets[i3] = InsertFingerprint(buckets[i3], entry_num, fingerprint);
            fingerprint = entry;

            // Пытаемся поместить fingerprint в другой bucket.
            i3 ^= HashFingerprint(fingerprint);

            // Если там уже есть такой fingerprint, то считаем, что вставка произошла.
            if (FindEntryInBucket(buckets[i3], fingerprint)) {
                return true;
            }

            entry_num = GetNumOfEmptyEntry(buckets[i3]);

            if (entry_num < b_ + 1) {
                InsertFingerprint(buckets[i3], entry_num, fingerprint);
                return true;
            }
        }

        return false;
    }
};

const int b = 4;
const int max_num_kicks = 500;

int main(int argc, char* argv[]) {
    // Проверка на наличие входных данных.
    if (argc < 3) {
        std::cout << "You haven't inserted input and output files.";
        return -1;
    }

    // Пути к файлам входных и выходных данных.
    std::string pathin = argv[1];
    std::string pathout = argv[2];


    try {
        std::ifstream fin{ pathin };
        std::ofstream fout{ pathout };

        // Проверка на существование файла входных данных.
        if (!fin) {
            std::cout << "There is no such input file.";
            return -2;
        }

        std::string videos;
        fin >> videos;

        // Читаем количество видео.
        int N = 0;
        fin >> N;

        fout << "Ok" << std::endl;

        std::unordered_map<std::string, std::unique_ptr<CuckooFilter>> user_filter;

        std::string command, user, video;
        while (fin >> command >> user >> video) {
            // Если у этого юзера ещё нет фильтра, то создаём его.
            if (user_filter.count(user) == 0) {
                user_filter.emplace(user, std::make_unique<CuckooFilter>(1.06 * N, b, max_num_kicks));
            }

            if (command == "watch") {
                user_filter[user]->Insert(video);
                fout << "Ok" << std::endl;
            }
            else {
                bool watched = false;
                watched = user_filter[user]->Lookup(video);
                fout << (watched ? "Probably" : "No") << std::endl;
            }
        }
    }
    catch (const std::exception& ex) {
        std::cout << ex.what() << "\n";
    }

    return 0;
}